---
output: github_document
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```


## Installation

You can install rlaScenarioViz with :

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/rlascenarioviz"
)
```

## Example

How to launch the shiny app

```{r example, eval = FALSE}
library(rlaScenarioViz)
run_app()
```
