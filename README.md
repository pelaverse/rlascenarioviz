
## Installation

You can install rlaScenarioViz with :

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/rlascenarioviz"
)
```

## Example

How to launch the shiny app

``` r
library(rlaScenarioViz)
run_app()
```
